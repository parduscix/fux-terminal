# Fux Terminal Emulatörü

  Python betik dili ile yazılmış VTE sanal terminal emulatörüne GTK-2.0 görsel kütüphanesi kullanılarak tasarlanmış bir terminal emulatörüdür.

  Görünümü standart olarak sistemin kendi pencere teması üzerine herhangi bir özel menüsü ve seçeneği olmayan, siyah arkaplan üstüne beyaz yazıdır. Tasarlanırken oldukça hafif olması ve sistemi kasmadan terminal ihtiyacını karşılaması amaçlanmıştır.
  
  Aşağıda ekran görüntüsünü görebilirsiniz.
  
![Alt text](https://github.com/fuxprojesi/fux-terminal/blob/master/fuxterm.png "Terminal Ekran Görüntüsü")

# Fux terminal emulatörü'nün özellikleri:

*  Sürükle-bırak ile dizin ve dosya konumunu satıra basar.
*  Standart olarak ctrl + shift + r kombinasyonu ile geriye doğru arama yapar.
*  Ev dizini ve Çöp konumu terminale sürüklendiğinde diğer terminallerin aksine tam konumunu ekrana basar.
*  Fare ile seçilen metni, fare orta tuşu ile ister terminale isterseniz herhangi bir belge veya tarayıcıya yapıştırır.
*  Standart bir terminaldir ve diğer tüm terminallerde yapılan herşey yapılabilir.
  
# Detaylı açıklama:
  Fux terminal diğer terminallerin aksine ctrl + shift + (c - v - x - z) kombinasyonları'nı desteklemez! Bu kombinasyonları kullanarak kopyala - yapıştır - kes - geri al  işlemlerini yapamazsınız. (NOT: Geliştirme sürecinde dahil edilmesi düşünülüyor)
  
  Terminale Sürükle-bırak ile her defasında sadece bir dizin veya dosya bırakabilirsiniz. Birden fazla dizin veya dosya sürüklediğinizde sadece ilk sıradaki'nin konumunu satıra basar.(NOT: Geliştirilme sürecinde dahil edilmesi düşünülüyor)
  
  Yukarıdaki dezavantajların haricinde normal bir terminale ev dizini'ni sürükleyip bıraktığınızda satıra şunun gibi (x-nautilus-desktop:///home) bir çıktı basar.
  
  Aynı olay Çöp içinde geçerlidir. Satıra şunun gibi (x-nautilus-desktop:///trash) bir çıktı basar.
  
  Fux terminale ev dizini ve çöp kutunuzu sürüklediğinizde, bu işlemleri ayıklayar ve tam olarak konumu ekrana basar.
  
  Aşağıda ekran görüntüsünü görebilirsiniz.
  
  ![Alt text](https://github.com/fuxprojesi/fux-terminal/blob/master/fuxterm2.png "Terminal İkinci Ekran Görüntüsü")
  
  Fux projesi'nin herhangi bir dağıtımını kullanıyorsanız, bu uygulamanın RPM paketi'ni DNF güncelleme yöneticisi'ni kullanarak terminal'den "sudo dnf install fux-terminal" komutunu kullanarak sisteminize kurabilirsiniz.
  
  Fux terminal Genel Kamu Lisansı ile birlikte dağıtılan özgür bir yazılımdır.
  
  
# Hızlı kurulum: 
root yetkisi alarak şu komutu çalıştırın:
  
cd /tmp/;wget https://raw.githubusercontent.com/parduscix/fux-terminal/master/fux-terminal.py ;mv fux-terminal.py /usr/bin/fux-terminal;wget https://raw.githubusercontent.com/parduscix/fux-terminal/master/fux-terminal.desktop;mv fux-terminal.desktop /usr/share/applications/fux-terminal.desktop;mkdir /usr/share/fux-terminal/;wget https://raw.githubusercontent.com/parduscix/fux-terminal/master/fux-terminal.png;mv fux-terminal.png /usr/share/fux-terminal/fux-terminal.png;chmod 755 /usr/bin/fux-terminal;chmod 755 /usr/share/applications/fux-terminal.desktop
